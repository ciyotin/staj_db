Create Database Veri;

USE Veri;

Create TABLE ogrenci(

numara int primary key not null,
isim varchar(15) not null,
soyisim varchar(15) not null,
ogretim ENUM('1', '2') not null default '1',
toplamKabulGun int default 0,
toplamGun int default 0,
stajS int default 0

);



Create Table komisyon(

uyeid int primary key not null, -- primary key

uyeisim varchar(50) not null,
uyesoyisim varchar(50) not null,
uyemail varchar(255) not null,
uyetelno char(11) not null,
password varchar(30) not null

);

insert into komisyon values(1 , "guzin","ulutas","guzin@ktu.edu","05111111111", 1111);

Create Table stajDonem(
mulakatNo int primary key not null,

ogrencino int,

KurumAdi varchar(30) not null,

basTarihi date not null,
bitTarihi date not null,
gunSayisi int not null,

sinif int not null,
kabulGun int not null default 0,
done ENUM('y', 'n') not null default 'n',

FOREIGN KEY fk_stajD(ogrencino)
   REFERENCES ogrenci(numara)
   ON UPDATE CASCADE
   ON DELETE no action
);

create Table kurum(
kurumAdi varchar(50) not null,
sehir varchar(20) not null
);

Create Table mulakat(

mulakatNo int,
uyeid int,

tarih date,
saat time,


devam int, -- 0-4 %4
cabaCalisma int, -- 0-4 %4
vakit int, -- 0-4 %4
amirDavranis int, -- 0-4 %4
isDavranis int ,  -- 0-4 %4
proje int, -- 0-100 %15
duzen int, -- 0-100 %5
sunum int, -- 0-100 %5
icerik int, -- 0-100 %15
mulakat int, -- 0-100 %40

basari int, -- basari 0-100

FOREIGN KEY fk_mulakat(mulakatNo)
   REFERENCES stajDonem(mulakatNo)
   ON UPDATE CASCADE
   ON DELETE no action
);




ALTER DATABASE Veri CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE kurum CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE komisyon CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE mulakat CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE ogrenci CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE stajDonem CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
