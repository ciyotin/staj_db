import java.sql.*;
 
/*
 * 
 * connection, statement, query
 * 
 */


public class JdbcDemo {

	public static void main(String[] args) throws SQLException {

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;
		
		String dbUrl = "jdbc:mysql://localhost:3306/Veri?useSSL=false";
		String user = "root";		
		String pass = "makarna";

		try {
			// 1. Get a connection to database
			myConn = DriverManager.getConnection(dbUrl, user, pass);
			
			// 2. Create a statement
			myStmt = myConn.createStatement();
			
			// 3. Execute SQL query
			myRs = myStmt.executeQuery("select * from ogrenci");
			
			// 4. Process the result set
			while (myRs.next()) {
				System.out.println(myRs.getString("isim") + " " + myRs.getString("soyisim") + " " + myRs.getString("ogretim") + " " + myRs.getString("toplamGun") );
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
		finally {
			if (myRs != null) {
				myRs.close();
			}
			
			if (myStmt != null) {
				myStmt.close();
			}
			
			if (myConn != null) {
				myConn.close();
			}
		}
	}

}