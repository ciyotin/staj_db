import java.sql.*;

/**
 * 
 * instert into database
 *
 */
public class JdbcInsertDemo {

	public static void main(String[] args) throws SQLException {

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;
		
		String dbUrl = "jdbc:mysql://localhost:3306/Veri?useSSL=false";
		String user = "root";		
		String pass = "makarna";

		try {
			// 1. Get a connection to database
			myConn = DriverManager.getConnection(dbUrl, user, pass);
			
			// 2. Create a statement
			myStmt = myConn.createStatement();

			// 3. Insert a new employee
			System.out.println("Inserting a new ogrenci to database\n");
			
			
			
			int rowsAffected = myStmt.executeUpdate(
				"insert into ogrenci " +
				"(isim, soyisim, numara, ogretim, toplamGun) " + 
				"values " + 
				"('yusa', 'aksu', '348320', '1', 30)");
			
			// 4. Verify this by getting a list of employees
			myRs = myStmt.executeQuery("select * from ogrenci order by soyisim");
			
			// 5. Process the result set
			while (myRs.next()) {
				System.out.println(myRs.getString("isim") + " " + myRs.getString("soyisim"));
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
		finally {
			if (myRs != null) {
				myRs.close();
			}
			
			if (myStmt != null) {
				myStmt.close();
			}
			
			if (myConn != null) {
				myConn.close();
			}
		}
	}

}
