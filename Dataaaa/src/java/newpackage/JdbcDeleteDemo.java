import java.sql.*;

/**
 * 
 * delete
 *
 */
public class JdbcDeleteDemo {

	public static void main(String[] args) throws SQLException {

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;
		
		String dbUrl = "jdbc:mysql://localhost:3306/Veri?useSSL=false";
		String user = "root";		
		String pass = "makarna";

		try {
			// Get a connection to database
			myConn = DriverManager.getConnection(dbUrl, user, pass);
			
			// Create a statement
			myStmt = myConn.createStatement();

			// Call helper method to display the employee's information
			System.out.println("BEFORE THE DELETE...");
			displayEmployee(myConn, "yusa", "aksu");
			
			// DELETE the employee
			System.out.println("\nDELETING THE EMPLOYEE: yusa aksu\n");
			
			int rowsAffected = myStmt.executeUpdate(
					"delete from ogrenci " +
					"where soyisim='aksu' and isim='yusa'");
			
			// Call helper method to display the employee's information
			System.out.println("AFTER THE DELETE...");
			displayEmployee(myConn, "yusa", "aksu");
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
		finally {
			close(myConn, myStmt, myRs);
		}
	}

	private static void displayEmployee(Connection myConn, String firstName, String lastName) throws SQLException {
		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			// Prepare statement
			myStmt = myConn
					.prepareStatement("select isim, soyisim, toplamGun from ogrenci where isim=? and soyisim=?");

			myStmt.setString(1, firstName);
			myStmt.setString(2, lastName);
			
			// Execute SQL query
			myRs = myStmt.executeQuery();

			// Process result set
			while (myRs.next()) {
				String theFirstName = myRs.getString("isim");
				String theLastName = myRs.getString("soyisim");
				String toplamGun = myRs.getString("toplamGun");
			
				System.out.printf("%s %s %s\n", theFirstName, theLastName, toplamGun);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		} finally {
			close(myStmt, myRs);
		}

	}

	private static void close(Connection myConn, Statement myStmt,
			ResultSet myRs) throws SQLException {
		if (myRs != null) {
			myRs.close();
		}

		if (myStmt != null) {
			myStmt.close();
		}

		if (myConn != null) {
			myConn.close();
		}
	}

	private static void close(Statement myStmt, ResultSet myRs)
			throws SQLException {

		close(null, myStmt, myRs);
	}	
}
