import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;


public class bellitarih {
	
	public static void main(String[] args) throws SQLException {

		Connection myConn = null;
		PreparedStatement myStmt = null;
		ResultSet myRs = null;
		
		String dbUrl = "jdbc:mysql://localhost:3306/Veri?useSSL=false";
		String user = "root";		
		String pass = "makarna";
		
		try {
			
			// 1. Get a connection to database
			myConn = DriverManager.getConnection(dbUrl , user , pass);
			
			// 2. Prepare statement
			myStmt = myConn.prepareStatement("select * from mulakat where tarih > ? and tarih < ?");
			
			// 3. Set the parameters
			myStmt.setString(1, "2018-08-08");
			
			myStmt.setString(2,"2028-08-08" );
						
			// 4. Execute SQL query
			myRs = myStmt.executeQuery();
	
			// 5. Display the result set
			display(myRs);
		
			//
			// Reuse the prepared statement:  salary > 25000,  department = HR
			//

			System.out.println("\n\nReuse the prepared statement:  toplamgun > 56");
			
			// 6. Set the parameters
			
			/*myStmt.setDouble(1, 601);
			 * 
			 * parametreyi setliyor tekrar 
			 * 
			 * */
			
			
			// 7. Execute SQL query
			myRs = myStmt.executeQuery();
			
			// 8. Display the result set
			display(myRs);


		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
		finally {
			if (myRs != null) {
				myRs.close();
			}
			
			if (myStmt != null) {
				myStmt.close();
			}
			
			if (myConn != null) {
				myConn.close();
			}
		}
	}

	private static void display(ResultSet myRs) throws SQLException {
		while (myRs.next()) {
			int mulakatno = myRs.getInt("mulakatNo");
			
			int ogrencino = myRs.getInt("ogrencino");
			String tarih  = myRs.getString("tarih");	
			
			System.out.printf("%d %d %s\n", mulakatno, ogrencino, tarih);
		}
	}

}
